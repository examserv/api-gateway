FROM golang:1.19-alpine
RUN mkdir api
COPY . /api
WORKDIR /api
RUN go mod tidy
RUN go mod vendor
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 9090

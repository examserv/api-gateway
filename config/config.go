package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment string

	CustomerServiceHost string
	CustomerServicePort int

	ReViewServiceHost string
	ReViewServicePort int

	PostServiceHost string
	PostServicePort int

	EmailServiceHost string
	EmailServicePort int

	CtxTimeout int
	LogLevel   string
	HTTPPort   string
	SignInKey  string

	RedisHost string
	RedisPort string

	PostgresHost     string
	PostgresPort     int
	PostgresUser     string
	PostgresDB       string
	PostgresPassword string
	AuthConfigPath   string
	CSVfilePath      string
}

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDB = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "customerdb"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "123"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":9090"))

	c.CustomerServiceHost = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToInt(getOrReturnDefault("CUSTOMER_SERVICE_PORT", 1111))

	c.PostServiceHost = cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "localhost"))
	c.PostServicePort = cast.ToInt(getOrReturnDefault("POST_SERVICE_PORT", 2222))

	c.ReViewServiceHost = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReViewServicePort = cast.ToInt(getOrReturnDefault("REVIEW_SERVICE_PORT", 3333))

	c.EmailServiceHost = cast.ToString(getOrReturnDefault("EMAIL_SERVICE_HOST", "email_service"))
	c.EmailServicePort = cast.ToInt(getOrReturnDefault("EMAIL_SERVICE_PORT", 5555))

	c.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST", "redis"))
	c.RedisPort = cast.ToString(getOrReturnDefault("REDIS_PORT", "6379"))

	c.SignInKey = cast.ToString(getOrReturnDefault("SIGNINGKEY", "jamesRodrigues"))
	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))

	c.AuthConfigPath = cast.ToString(getOrReturnDefault("AUTH_PATHDB", "./config/auth.conf"))
	c.CSVfilePath = cast.ToString(getOrReturnDefault("AUTH_PATH", "./config/auth.csv"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}

package v1

import (
	"api-gateway/config"
	"api-gateway/pkg/logger"
	"api-gateway/services"
	"api-gateway/storage/repo"

	t "api-gateway/api/tokens"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	cfg            config.Config
	redis          repo.InMemoryStorageI
	jwtHandler     t.JWTHandler
}

// HandlerV1Config ...
type HandlerV1Config struct {
	Logger           logger.Logger
	ServiceManager   services.IServiceManager
	Cfg              config.Config
	InMermoryStorage repo.InMemoryStorageI
	JwtHandler       t.JWTHandler
}

// New ...
func New(c *HandlerV1Config) *handlerV1 {
	return &handlerV1{
		log:            c.Logger,
		serviceManager: c.ServiceManager,
		cfg:            c.Cfg,
		redis:          c.InMermoryStorage,
		jwtHandler:     c.JwtHandler,
	}
}

func GetClaims(h handlerV1, c *gin.Context) (*t.CustomClaims, error) {

	var (
		claims = t.CustomClaims{}
	)

	strToken := c.GetHeader("Authorization")

	token, err := jwt.Parse(strToken, func(t *jwt.Token) (interface{}, error) { return []byte(h.cfg.SignInKey), nil })

	if err != nil {
		h.log.Error("invalid access token")
		return nil, err
	}
	rawClaims := token.Claims.(jwt.MapClaims)

	claims.Sub = rawClaims["sub"].(string)
	claims.Exp = rawClaims["exp"].(float64)
	aud := cast.ToStringSlice(rawClaims["aud"])
	claims.Aud = aud
	claims.Role = rawClaims["role"].(string)
	claims.Token = token
	return &claims, nil
}

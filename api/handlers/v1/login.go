package v1

import (
	"api-gateway/api/models"
	pbu "api-gateway/genproto/customer"
	"api-gateway/pkg/etc"
	"api-gateway/pkg/logger"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary login
// @Description 	this gets LogIn
// @Tags Auth
// @Accept json
// @Produce json
// @Param 			email 		path string true "email"
// @Param 			password 	path string true "password"
// @Success 200 	{object} 	models.Login
// @Failure 400 	{object} 	models.Error
// @Router /login/{email}/{password} [get]
func (h *handlerV1) LogInUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		email    = c.Param("email")
		password = c.Param("password")
	)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	user, err := h.serviceManager.Customer().LogIn(ctx, &pbu.LoginRequest{Email: email})
	if err != nil {
		h.log.Error("error while logging into ", logger.Error(err))

		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Something went wrong, please try again",
		})
		return
	}
	hashPass, err := etc.HashPassword(password)
	// if err != nil {
	// 	c.JSON(http.StatusNotFound, models.Error{
	// 		Description: "error while password hashing",
	// 		Code:        http.StatusBadRequest,
	// 	})
	// 	return
	// }
	fmt.Println(hashPass)
	fmt.Println(user.Password)
	fmt.Println(hashPass == user.Password)
	// if hashPass != user.Password {
	// 	c.JSON(http.StatusNotFound, models.Error{
	// 		Description: "Password wrong",
	// 		Code:        http.StatusBadRequest,
	// 	})
	// 	return
	// }

	h.jwtHandler.Iss = "user"
	h.jwtHandler.Sub = user.Id
	h.jwtHandler.Role = "authorized"
	h.jwtHandler.Aud = []string{"project-app"}
	h.jwtHandler.SignInKey = h.cfg.SignInKey
	h.jwtHandler.Log = h.log
	tokens, err := h.jwtHandler.GenerateAuthJWT()
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	refreshToken := tokens[1]
	accessToken := tokens[0]
	user.RefreshToken = refreshToken
	response := &models.Login{
		Id:          user.Id,
		Email:       user.Email,
		LastName:    user.LastName,
		FirstName:   user.FirstName,
		Bio:         user.Bio,
		Password:    user.Password,
		PhoneNumber: user.PhoneNumber,
	}
	response.Refreshtoken = refreshToken
	response.Accessestoken = accessToken
	c.JSON(http.StatusOK, response)
}

package v1

import (
	"api-gateway/api/models"
	"api-gateway/genproto/customer"
	"api-gateway/pkg/logger"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/spf13/cast"
	"google.golang.org/protobuf/encoding/protojson"
)

// Verify customer
// @Summary      Verify customer
// @Description  Verifys customer
// @Tags         Customer
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        code   path string true "code"
// @Success      200  {object}  models.VerifyResponse
// @Router      /verify/{email}/{code} [get]
func (h *handlerV1) Verification(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	var (
		code  = c.Param("code")
		email = c.Param("email")
	)
	customerBody, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting customer from redis", logger.Any("redis", err))
		return
	}
	body := customer.CustomerReq{}
	csBodys := cast.ToString(customerBody)
	err = json.Unmarshal([]byte(csBodys), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to customer body", logger.Any("json", err))
		return
	}
	if body.Code != code {
		fmt.Println(body.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}
	id := uuid.New()
	body.Id = id.String()

	h.jwtHandler.Iss = "user"
	h.jwtHandler.Sub = body.Id
	h.jwtHandler.Role = "authorized"
	h.jwtHandler.Aud = []string{"exam-app"}
	h.jwtHandler.SignInKey = h.cfg.SignInKey
	h.jwtHandler.Log = h.log
	_, err = json.MarshalIndent(h.jwtHandler, "", "    ")
	if err != nil {
		h.log.Error("error occured while Marshaling jwthandler")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong, please try again",
		})
		return
	}
	tokens, err := h.jwtHandler.GenerateAuthJWT()
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong, please try again",
		})
		return
	}
	accessToken := tokens[0]
	refreshToken := tokens[1]
	body.RefreshToken = refreshToken
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	res, err := h.serviceManager.Customer().CreateCustomer(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer", logger.Any("post", err))
		return
	}

	response := &models.VerifyResponse{
		Id:          res.Id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Email:       res.Email,
		Bio:         res.Bio,
		PhoneNumber: res.PhoneNumber,
	}
	response.JWT = accessToken
	response.RefreshToken = refreshToken

	c.JSON(http.StatusOK, response)
}

package v1

import (
	"api-gateway/api/models"
	"api-gateway/email"
	pc "api-gateway/genproto/customer"
	"api-gateway/pkg/etc"
	"api-gateway/pkg/logger"
	"api-gateway/pkg/utils"
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"golang.org/x/crypto/bcrypt"
)

// register customer
// @Summary 		register customer
// @Description 	this registers customer
// @Tags 			Customer
// @Accept 			json
// @Produce         json
// @Param           registercustomer       body  	models.CustomerCreat true "customer"
// @Success         200					  {object}  models.Error
// @Failure         500                   {object}  models.Error
// @Router          /register [post]
func (h *handlerV1) RegisterCustomer(c *gin.Context) {
	var body models.CustomerCreat

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, models.Error{
			Error: err,
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
		return
	}
	_, err = utils.IsValidMail(body.Email)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid email address",
		})
		return
	}

	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	exists, err := h.serviceManager.Customer().CheckField(ctx, &pc.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		h.log.Error("error while checking username existance", logger.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error while cheking username existance",
		})
		return
	}
	if exists.Exists {
		c.JSON(http.StatusOK, gin.H{
			"message": "such username already exists",
		})
		return
	}
	existsUser, err := h.redis.Exists(body.Email)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
			"info":  "please enter another email",
		})
		h.log.Error("failed check email uniques", logger.Error(err))
		return
	}
	if cast.ToInt(existsUser) == 1 {
		c.JSON(http.StatusConflict, gin.H{
			"error": err.Error(),
		})
		return
	}

	body.Password, err = etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,
		})
		h.log.Error("couldn't hash the password")
		return
	}
	hashPass, err := bcrypt.GenerateFromPassword([]byte(body.Password), 15)
	if err != nil {
		h.log.Error("error while hashing password", logger.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong",
		})
		return
	}
	body.Password = string(hashPass)
	code := strconv.Itoa(utils.RandomNum(6))

	response := &models.Customer{
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Bio:         body.Bio,
		Email:       body.Email,
		PhoneNumber: body.PhoneNumber,
		Password:    body.Password,
		Code:        code,
	}
	msg := "Subject: Exam email verification\n Your verification code is your email: " + response.Code
	err = email.SendEmail([]string{response.Email}, []byte(msg))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       nil,
			Code:        http.StatusAccepted,
			Description: "Your Email is not valid, Please recheck it",
		})
		return
	}

	jsNewCustomer, err := json.Marshal(response)
	if err != nil {
		h.log.Error("error while marshaling new user, inorder to insert it to redis", logger.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error while creating user",
		})
		return
	}

	err = h.redis.SetWithTTL(string(body.Email), string(jsNewCustomer), 600)
	if err != nil {
		h.log.Error("error while inserting new user into redis")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong, please try again",
		})
		return
	}

	c.JSON(http.StatusAccepted, gin.H{
		"info": "Your request succesfulley created, YourCode is : " + code,
	})

}

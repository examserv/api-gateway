package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"api-gateway/api/models"
	ps "api-gateway/genproto/post"
	"api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create post
// @Summary 		Create post
// @Description 	this creates post
// @Tags 			Post
// @Accept 			json
// @Security 		BearerAuth
// @Produce         json
// @Param           post        body  	 post.PostReq true "post"
// @Success         201					  {object} 	post.PostResp
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts [post]
func (h *handlerV1) CreatePost(c *gin.Context) {
	var (
		body        *ps.PostReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Post().CreatePost(ctx, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while creating post", logger.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// Update post
// @Summary 		Update post
// @Description 	this Updates post
// @Tags 			Post
// @Security 		BearerAuth
// @Accept 			json
// @Produce         json
// @Param           post        body  	  post.PostReq true "post"
// @Success         200					  {object} 	post.PostResp
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts [put]
func (h *handlerV1) UpdatePost(c *gin.Context) {
	var (
		body        models.UpdatePost
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	postToBeUpdated := &ps.PostReq{
		UserId:      body.UserId,
		Name:        body.Name,
		Description: body.Description,
	}
	for _, m := range body.Medias {
		postToBeUpdated.Medias = append(postToBeUpdated.Medias, &ps.Media{
			Id:     m.Id,
			PostId: m.PostId,
			Name:   m.Name,
			Link:   m.Link,
			Type:   m.Type,
		})
	}
	response, err := h.serviceManager.Post().UpdatePost(ctx, postToBeUpdated)

	// response, err := h.serviceManager.Post().UpdatePost(ctx, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while updating post", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// Delete post
// @Summary 		Delete post
// @Description 	this deletes post
// @Tags 			Post
// @Accept 			json
// @Produce         json
// @Security 		BearerAuth
// @Param           id        query  int  true "id"
// @Success         200					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts/{id} [delete]
func (h *handlerV1) DeletePost(c *gin.Context) {
	var (
		body        ps.PostId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.Post().DeletePostId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while deleting post", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.Error{
		Error:       nil,
		Code:        http.StatusOK,
		Description: "post just has deleted",
	})
}

// Get post
// @Summary 		Get post
// @Description 	this gets post
// @Tags 			Post
// @Accept 			json
// @Security 		BearerAuth
// @Produce         json
// @Param           id        query  int  true "id"
// @Success         200					  {object} 	post.PostResp
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts/{id} [get]
func (h *handlerV1) GetPostId(c *gin.Context) {
	var (
		body        ps.PostId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Query("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while converting id in getting customer", logger.Error(err))
		return
	}
	body.Id = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Post().GetPostId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting post by id", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get posts by user id
// @Summary 		Get posts by user id
// @Description 	this gets posts of specific user
// @Tags 			Post
// @Accept 			json
// @Produce         json
// @Security 		BearerAuth
// @Param           id        query  int  true "id"
// @Success         200					  {object} 	post.Posts
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts/userid/{id} [get]
func (h *handlerV1) GetPostUserId(c *gin.Context) {
	var (
		body        ps.PostUserId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Query("id")
	// id, err := strconv.ParseInt(guid, 10, 64)
	// if err != nil {
	// 	c.JSON(http.StatusInternalServerError, models.Error{
	// 		Code:        http.StatusInternalServerError,
	// 		Error:       err,
	// 		Description: "Not found",
	// 	})
	// 	h.log.Error("Error while converting id in getting customer", logger.Error(err))
	// 	return
	// }
	body.UserId = guid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Post().GetPostUserId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting post by owner id", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

package models

type Error struct {
	Code        int
	Error       error
	Description string
}
type ListUserReq struct {
	Page  int64
	Limit int64
}
type VerifyResponse struct {
	Id           string `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Email        string `json:"email"`
	Bio          string `json:"bio"`
	PhoneNumber  string `json:"phone_number"`
	JWT          string `json:"jwt"`
	RefreshToken string `json:"refresh"`
}

type AddressResponse struct {
	Id       int64  `json:"id"`
	UserId   int64  `json:"user_id"`
	District string `json:"district"`
	Street   string `json:"street"`
}
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
type CustomerCreat struct {
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Bio         string `json:"bio"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
	Password    string `json:"password"`
}

type Customer struct {
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	Email        string `json:"email"`
	Password     string `json:"password"`
	PhoneNumber  string `json:"phone_number"`
	Bio          string `json:"bio"`
	Code         string `json:"code"`
	Refreshtoken string `json:"refreshToken"`
}
type Login struct {
	Email         string
	FirstName     string
	LastName      string
	Password      string
	Bio           string
	PhoneNumber   string
	Refreshtoken  string
	Accessestoken string
	Id            string
}
type Admin struct {
	Id          string
	Password    string
	Name        string
	Email       string
	AccessToken string
}

package api

import (
	_ "api-gateway/api/docs" //swag
	v1 "api-gateway/api/handlers/v1"
	"api-gateway/config"
	"api-gateway/pkg/logger"
	"api-gateway/services"
	"api-gateway/storage/repo"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.InMemoryStorageI
}

// 35.78.91.102:9090
// @title           exam api
// @version         1.0
// @description     This is exam server api server
// @termsOfService  2 term exam
// @host      localhost:9090

// @BasePath  /v1
// @securityDefinitions.apikey  BearerAuth
// @in header
// @name Authorization

func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowHeaders:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowCredentials: true,
		AllowOrigins:     []string{},
	}))

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:           option.Logger,
		ServiceManager:   option.ServiceManager,
		Cfg:              option.Conf,
		InMermoryStorage: option.Redis,
	})

	api := router.Group("/v1")
	api.GET("/admin/login/:admin_name/:password", handlerV1.LoginAdmin)
	api.GET("/moderator/login/:name/:password", handlerV1.LoginModerator)
	api.GET("/login/:email/:password", handlerV1.LogInUser)
	api.POST("/register", handlerV1.RegisterCustomer)
	api.GET("/verify/:email/:code", handlerV1.Verification)
	// Customer
	api.POST("/customers", handlerV1.CreateCustomer)
	api.DELETE("/customers/:id", handlerV1.DeleteCustomer)
	api.GET("/customers/:id", handlerV1.GetCustomer)
	api.PUT("/customers", handlerV1.UpdateCustomer)
	api.GET("/customers/all", handlerV1.GetCustomers)
	api.GET("/customers/list:page/:limit", handlerV1.ListCustomers)
	api.GET("/customers/search:page/:limit/:key/:value", handlerV1.ListCustomers)

	// Posts
	api.POST("/posts", handlerV1.CreatePost)
	api.DELETE("/posts/post/:id", handlerV1.DeletePost)
	api.PUT("/posts", handlerV1.UpdatePost)
	api.GET("/posts/:id", handlerV1.GetPostId)
	api.GET("/posts/userid/:id", handlerV1.GetPostUserId)

	// Reviews
	api.POST("/reviews", handlerV1.CreateReveiw)
	api.PUT("/reviews", handlerV1.UpdateReview)
	api.GET("/reviews/post/:id", handlerV1.GetRewPostId)
	api.DELETE("/reviews/postid/:id", handlerV1.DeleteReviewPostId)
	api.DELETE("/reviews/userid/:id", handlerV1.DeleteReviewUserId)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return router
}

package redis

import (
	"api-gateway/storage/repo"

	"github.com/gomodule/redigo/redis"
	"github.com/spf13/cast"
)

type RedisRepo struct {
	Rdb *redis.Pool
}

func NewRedisRepo(rdb *redis.Pool) repo.InMemoryStorageI {
	return &RedisRepo{
		Rdb: rdb,
	}
}
func (r *RedisRepo) Exists(key string) (interface{}, error) {
	conn := r.Rdb.Get()
	defer conn.Close()
	return conn.Do("EXISTS", key)
}
func (r *RedisRepo) SetWithTTL(key, value string, duration int64) error {

	conn := r.Rdb.Get()
	defer conn.Close()

	_, err := conn.Do("SETEX", key, duration, value)
	return err
}

func (r *RedisRepo) Get(key string) (string, error) {
	conn := r.Rdb.Get()
	defer conn.Close()

	tem, err := conn.Do("GET", key)
	res := cast.ToString(tem)
	return res, err
}

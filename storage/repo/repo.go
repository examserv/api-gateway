package repo

type InMemoryStorageI interface {
	SetWithTTL(key, value string, duration int64) error
	Exists(key string) (interface{}, error)
	Get(key string) (string, error)
}

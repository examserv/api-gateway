pull_submodule:
	git submodule update --init --recursive

submod_up:
	git submodule update --remote --merge

run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:exam23/protos.git

proto-gen:
	./script/gen-proto.sh

migrate_up:
	migrate -path file://migrations/ -database postgres://jamshidbek:12345@database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com:5432/customerdb_jamshidbek up

migrate_down:
	migrate -path file://migrations/ -database jamshidbek:12345@database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com:5432/customerdb_jamshidbek down

migrate_force:
	migrate -path file://migrations/ -database jamshidbek:12345@database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com:5432/customerdb_jamshidbek force 1

swag:
	swag init -g ./api/router.go -o ./api/docs

